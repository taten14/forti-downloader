#Requires -RunAsAdministrator

Write-Host("Instalador de FortiVPN. Tribunal de Cuentas de la Provincia de Cordoba
Comenzando la instalacion...")

if (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {

    Set-Variable -Name "ARCH" -Value (Get-WmiObject Win32_OperatingSystem).OSArchitecture -Scope script
    Set-Variable -Name "HOSTNAME" -Value (Get-WmiObject Win32_OperatingSystem).CSName -Scope script
    Set-Variable -Name "ANTIVIRUS" -Value (Get-CimInstance -Namespace root/SecurityCenter2 -ClassName AntivirusProduct) -Scope script
    Set-Variable -Name "OS" -Value (Get-WmiObject -class Win32_OperatingSystem).Caption -Scope script
#    Set-Variable -Name "A32" -Value "https://d3gpjj9d20n0p3.cloudfront.net/forticlient/downloads/FortiClientVPNSetup_6.2.6.0951.exe" -Scope script
#    Set-Variable -Name "A64" -Value "https://d3gpjj9d20n0p3.cloudfront.net/forticlient/downloads/FortiClientVPNSetup_6.2.6.0951_x64.exe" -Scope script
    Write-Host("Comprobando instalacion previa...")
    Set-Variable -Name "App" -Value (Get-WmiObject -Class Win32_Product | Where-Object{$_.Name -eq "FortiClient VPN"}) -Scope script


    switch ($ANTIVIRUS.productState) { 
        "262144" {$UpdateStatus = "Up to date" ;$RealTimeProtectionStatus = "Disabled"} 
        "262160" {$UpdateStatus = "Out of date" ;$RealTimeProtectionStatus = "Disabled"} 
        "266240" {$UpdateStatus = "Up to date" ;$RealTimeProtectionStatus = "Enabled"} 
        "266256" {$UpdateStatus = "Out of date" ;$RealTimeProtectionStatus = "Enabled"} 
        "393216" {$UpdateStatus = "Up to date" ;$RealTimeProtectionStatus = "Disabled"} 
        "393232" {$UpdateStatus = "Out of date" ;$RealTimeProtectionStatus = "Disabled"} 
        "393488" {$UpdateStatus = "Out of date" ;$RealTimeProtectionStatus = "Disabled"} 
        "397312" {$UpdateStatus = "Up to date" ;$RealTimeProtectionStatus = "Enabled"} 
        "397328" {$UpdateStatus = "Out of date" ;$RealTimeProtectionStatus = "Enabled"} 
        "397584" {$UpdateStatus = "Out of date" ;$RealTimeProtectionStatus = "Enabled"} 
        "397568" {$UpdateStatus = "Up to date"; $RealTimeProtectionStatus = "Enabled"}
        "393472" {$UpdateStatus = "Up to date" ;$RealTimeProtectionStatus = "Disabled"}
    default {$UpdateStatus = "Unknown" ;$RealTimeProtectionStatus = "Unknown"} 
    }



    if ($App.Name -eq "FortiClient VPN") {
        Write-Host("Aplicacion ya instalada con la version $($App.Version) Saliendo...
    Presione una tecla para continuar....")
        [void][System.Console]::ReadKey($true)
        Exit
    }else{
        
        Write-Host("No se ha encontrado la aplicacion. Procedemos con la insalacion....")
        Set-Variable -Name "DESKTOP_FOLDER" -Value $([Environment]::GetFolderPath("Desktop")) -Scope script
        Set-Variable -Name "SGE_LOCATION" -Value "$DESKTOP_FOLDER\SGE.lnk" -Scope script
        Set-Variable -Name "SUAF_LOCATION" -Value "$DESKTOP_FOLDER\SUAF.lnk" -Scope script
        Set-Variable -Name "SUAC_LOCATION" -Value "$DESKTOP_FOLDER\SUAC.lnk" -Scope script

        $ExplorerPath="$((get-command explorer.exe).Path)"

        $SGEShell = New-Object -ComObject ("WScript.Shell")
        $SGEShortcut = $SGEShell.CreateShortcut($SGE_LOCATION)
        $SGEShortcut.TargetPath=$ExplorerPath
        $SGEShortcut.Arguments="http://tribu01:8180/sge/servlet/hloginadm"
        $SGEShortcut.Save()

        $SUAFShell = New-Object -ComObject WScript.Shell
        $SUAFShortcut = $SUAFShell.CreateShortcut($SUAF_LOCATION)
        $SUAFShortcut.TargetPath = $ExplorerPath
        $SUAFShortcut.Arguments = "http://suaf.cba.gov.ar/"
        $SUAFShortcut.Save()

        $SUACShell = New-Object -ComObject WScript.Shell
        $SUACShortcut = $SUACShell.CreateShortcut($SUAC_LOCATION)
        $SUACShortcut.TargetPath = $ExplorerPath
        $SUACShortcut.Arguments = "http://suac.cba.gov.ar/"
        $SUACShortcut.Save()
    }

    Write-Host("Hostname: $HOSTNAME")
    Write-Host("Sistema Operativo: $OS")
    Write-Host("Arquitectura:  $ARCH")
    Write-Host("Antivirus: $($ANTIVIRUS.displayName) | Estado Actualizacion: $UpdateStatus | Estado: $RealTimeProtectionStatus")

    if ( $ARCH -eq "64 bits") {
        Write-Host("Comenzando la instalacion de la aplicacion (amd64). Paciencia....")
        #Invoke-WebRequest -Uri $A64 -OutFile "forticlient.exe" -verbose -TimeoutSec 120
        Write-Host("Instalando FortiVPN")
        Start-Process .\exe\FortiClientVPNSetup_6.2.6.0951_x64.exe -Wait
        }else{
        Write-Host("Comenzando la instalacion de la aplicacion (x86). Paciencia....")
        #Invoke-WebRequest -Uri $A32 -OutFile "forticlient.exe" -verbose
        Write-Host("Instalando FortiVPN")
        Start-Process .\exe\FortiClientVPNSetup_6.2.6.0951_intel.exe -Wait
        }
    $FORTI_PATH = "${env:ProgramFiles}\Fortinet\FortiClient"
    if ((Get-WmiObject -Class Win32_Product | Where-Object{$_.Name -eq "FortiClient VPN"}).Name -eq "FortiClient VPN") {
        Write-Host("La aplicacion se instalo exitosamente...
Actualizando configuracion de Forti CLient")
    Start-Process "$FORTI_PATH\FCConfig.exe" -Args "-m all -f .\files\user.conf -o import -i 1" -Wait
    Write-Host("Actualizando el archivo Hosts")
    Add-Content -Path C:\Windows\System32\drivers\etc\hosts -Value "172.16.10.100`ttribu01" -Force
    Write-Host("Aplicacion instalada. Presione cualquier tecla para continuar...")
    [void][System.Console]::ReadKey($true)
        Exit
    }else{
        Write-Host("La aplicacion no se ha onstalado correctamente. Ponete en contacto con Operaciones")
        Write-Host("Hostname: $HOSTNAME")
        Write-Host("Sistema Operativo: $OS")
        Write-Host("Arquitectura:  $ARCH")
        Write-Host("Antivirus: $($ANTIVIRUS.displayName) | Estado Actualizacion: $UpdateStatus | Estado: $RealTimeProtectionStatus")
        [void][System.Console]::ReadKey($true)
        Exit
    }
}else{
    Write-Host("El programa requiere permisos de administrador. 
Hace click derecho sobre el archivo y hace click en 'Ejecutar como Administrador'")
    [void][System.Console]::ReadKey($true)
    Exit
}